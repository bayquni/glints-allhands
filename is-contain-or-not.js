function check(word1, word2) {

    let data = {} 

    for (let i = 0; i < word2.length; i++) {
        if (data[word2[i]]) {
            data[word2[i]]++
        } else {
            data[word2[i]] = 1
        }
    }

    for (let j = 0; j < word1.length; j++) {
        if (data[word1[j]]) {
            data[word1[j]]--
        } else {
            return false
        }


    }

    console.log(data)
    return true

}

console.log(check("hello", "hello world"))
console.log(check('sing', 'sting'))
console.log(check('abc', 'abracadabra'))
console.log(check('abc', 'acd'))
