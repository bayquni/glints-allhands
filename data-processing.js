
function hitung(data) {
    avg = (data[0] + data[1] + data[2] + data[3]) / data.length 
    graduated = 0;
    ungraduated = 0;

    for (let i = 0; i < data.length; i++) {
        if (data[i] > 50) {
            graduated += 1
        } else {
            ungraduated += 1
        }
    }

    lulus = (100 / data.length) * graduated
    
    if (lulus >= 70) {
        status = 'Good Performance'
    } else {
        status = 'Poor Performance'
    }

    return {
        "status" : `${status}`,
        "reports" : {
                "avarage_score" : `${avg}`,
                "graduated" : `${graduated}`,
                "ungraduated" : `${ungraduated}`
        }   
    }
}

console.log(hitung([60, 60, 60, 50]))
console.log(hitung([60, 60, 60, 50, 34, 23, 68, 75]))