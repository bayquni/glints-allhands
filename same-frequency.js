function freq(data1, data2) {
    data1 = data1.toString()
    data2 = data2.toString()

    if (data1.length !== data2.length) {
        return false
    }

    for (let i = data1.length - 1; i >= data1.length / 2; i--) {
        if (data1[i] !== data2[data2.length - i - 1]) {
            return false
        }
    }

    return true

}

console.log(freq(1821, 1281))
console.log(freq(18, 21))
console.log(freq(2, 222))
console.log(freq(1234, 4321))

